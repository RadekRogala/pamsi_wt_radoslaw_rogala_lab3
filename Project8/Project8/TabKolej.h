#pragma once

#include "wyjatki.h"

template <class typ>
class TabKolej
{
	int pojemnosc;
	typ *kolej;
	int gora;

public:


	TabKolej(int p) :pojemnosc(p) {
		kolej = new typ[pojemnosc];
		gora = 2;

	}
	~TabKolej() {
		if (!isEmpty())
			delete[] kolej;
	}

	//sprawdza czy kolejka jest pusta
	//true - jest pusta
	//false - nie jest pusta
	bool isEmpty() const {
		return gora < 0;
	}


	//dodaje elelemnt do kolejki lub wyrzuca wy�tek FullStackException kiedy kolejka jest pe�na
	void dodaj(const typ & wart);

	//usuwa element z pocztaku kolejki lub wyrzuca EmptyStackExceptionkiedy kolejka jest pusta
	typ usun();

	//zwraca ilo�� element�w w kolejce
	int size() const {
		return gora + 1;
	}

	//wyswietla wszystkie elementy na standardowe wyjscie
	void show() const;

	//usuwa wszystkie elementy kolejki
	void deleteAll();
};



template <class typ>
void TabKolej<typ>::dodaj(const typ & wart) {
	//if (gora == pojemnosc - 1) {
	//	FullStackExcpetion fullE;
	//	throw fullE;
	//}
	kolej[gora] = wart;
	gora=gora--;
}

template <class typ>
typ TabKolej<typ>::usun() {
	if (isEmpty()) {
		EmptyStackException emptyE;
		throw emptyE;
	}
	return kolej[gora++];
}

template <class typ>
void TabKolej<typ>::show() const {
	for (int i = 0; i <= gora; i++)
		cout << kolej[i] << endl;
}

template <class typ>
void TabKolej<typ>::deleteAll() {
	if (isEmpty()) {
		EmptyStackException emptyE;
		throw emptyE;
	}
	gora = 2;
	delete[] kolej;
}
