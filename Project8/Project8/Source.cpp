
#include <iostream>
#include <exception>
#include "TabKolej.h"
#include "StlKolej.h"

using namespace std;

int main()
{
	TabKolej <int> s(3);
	try
	{
		s.dodaj(5);
		s.dodaj(3);
		s.dodaj(4);
		s.show();
		cout << endl;
		cout << "jestem" << endl;
		s.dodaj(1);
		cout << "jestem2" << endl;
	}
	catch (exception & e)
	{
		cerr << e.what() << endl;
	}
	try
	{
		cout << s.usun() << endl;
		s.show();
		cout << s.usun() << endl;
		cout << s.usun() << endl;
		cout << "jestem" << endl;
		cout << s.usun() << endl;
		cout << "jestem2" << endl;
	}
	catch (exception & e)
	{
		cerr << e.what() << endl;
	}


	cout << endl;

	StlKolej <int> s1;

	try
	{
		s1.push(5);
		s1.push(3);
		s1.push(4);
		s1.show();
		cout << "jestem" << endl;
		s1.push(1);
		cout << "jestem2" << endl;

	}
	catch (exception & e)
	{
		cerr << e.what() << endl;
	}
	try
	{
		s1.show();
		cout << s1.pop() << endl;
		cout << s1.pop() << endl;
		cout << "jestem" << endl;
		s1.show();
		cout << "jestem2" << endl;

	}
	catch (exception & e)
	{
		cerr << e.what() << endl;
	}
	cin.get();
	return 0;
}

