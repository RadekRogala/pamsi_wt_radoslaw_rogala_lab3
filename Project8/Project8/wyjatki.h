#pragma once
#include <exception>

class EmptyStackException : public std::exception
{
public:
	virtual const char * what() const noexcept
	{
		return "Kolejka jest pusta";
	}
};

class FullStackExcpetion : public std::exception
{
public:
	virtual const char * what() const noexcept
	{
		return "Kolejka jest pelna";
	}
};

