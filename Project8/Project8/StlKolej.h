#pragma once

#pragma once
#include <queue>
#include "wyjatki.h"

using namespace std;

template <class typ>
class StlKolej
{
	queue <typ> kolej;
public:
	//sprawdza czy kolejka jest pusta
	//true - jest pusta
	//false - nie jest pusta
	bool isEmpty() {
		return kolej.empty();
	}

	typ & front() const {
		return kolej.front();
	}

	//dodaje elelemnt do kolejki lub wyrzuca wy�tek FullStackException kiedy kolejka jest pe�na
	void push(const typ & wart) {
		kolej.push(wart);
	}

	//�ciaga element z kolejki lub wyrzuca wyj�tek EmptyStackException kiedy kolejka jest pusta
	typ pop();

	//zwraca ilo�� element�w w kolejce
	int size() {
		return kolej.size();
	}

	//wyswietla ca�a kolejke na standardowym wyj�ciu
	void show();

	//usuwa wszystkie elementy z kolejki
	void deleteAll() {
		while (!kolej.empty())
			kolej.pop();
	}
};

template <class typ>
typ StlKolej<typ>::pop() {
	if (kolej.empty()) {
		EmptyStackException wyjatek;
		throw wyjatek;
	}

	kolej.pop();

}

template <class typ>
void StlKolej<typ>::show() {
	queue <typ> tmp = kolej;
	while (!tmp.empty())
	{
		cout << tmp.front() << endl;
		tmp.pop();
	}
}

