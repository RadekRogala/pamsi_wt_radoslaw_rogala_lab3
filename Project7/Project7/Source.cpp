
#include <iostream>
#include <exception>
#include "TabStos.h"
#include "StlStos.h"

using namespace std;

int main()
{
	TabStos <int> s(3);
	try
	{
		s.push(5);
		s.push(3);
		s.push(4);
		s.show();
		cout << endl;
		cout << "jestem" << endl;
		s.push(1);
		cout << "jestem2" << endl;
	}
	catch (exception & e)
	{
		cerr << e.what() << endl;
	}
	try
	{
		cout << s.pop() << endl;
		cout << s.pop() << endl;
		cout << s.pop() << endl;
		cout << "jestem" << endl;
		cout << s.pop() << endl;
		cout << "jestem2" << endl;
	}
	catch (exception & e)
	{
		cerr << e.what() << endl;
	}


	cout << endl;

	StlStos <int> s1;

	try
	{
		s1.push(5);
		s1.push(3);
		s1.push(4);
		s1.show();
		cout << "jestem" << endl;
		s1.push(1);
		cout << "jestem2" << endl;

	}
	catch (exception & e)
	{
		cerr << e.what() << endl;
	}
	try
	{
		cout << s1.pop() << endl;
		cout << s1.pop() << endl;
		cout << s1.pop() << endl;
		cout << s1.pop() << endl;
		cout << "jestem" << endl;
		cout << s1.pop() << endl;
		cout << "jestem2" << endl;

	}
	catch (exception & e)
	{
		cerr << e.what() << endl;
	}
	cin.get();
	return 0;
}

