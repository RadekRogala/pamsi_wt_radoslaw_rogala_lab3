#pragma once

#include "wyjatki.h"

template <class typ>
class TabStos
{
	int pojemnosc;
	typ *stos;
	int gora;

public:
	TabStos(int p) :pojemnosc(p) {
		stos = new typ[pojemnosc];
		gora = -1;
	}
	~TabStos() {
		if (!isEmpty())
			delete[] stos;
	}

	//sprawdza czy stos jest pusty
	//true - jest pusty
	//false - nie jest pusty
	bool isEmpty() const {
		return gora < 0;
	}

	//zwraca element g�ry stosu lub wyrzuca wyj�tek EmptyStackException kiedy stos jest pusty
	typ & top() const;

	//dodaje elelemnt do stosu lub wyrzuca wy�tek FullStackException kiedy stos jest pe�ny
	void push(const typ & wart);

	//�ciaga element ze stosu lub wyrzuca wyj�tek EmptyStackException kiedy stos jest pusty
	typ pop();

	//zwraca ilo�� element�w na stosie
	int size() const {
		return gora + 1;
	}

	//wyswietla wszystkie elementy na standardowe wyjscie
	void show() const;

	//usuwa wszystkie elementy stosu
	void deleteAll();
};


template <class typ>
typ & TabStos<typ>::top() const {
	if (isEmpty()) {
		EmptyStackException emptyE;
		throw emptyE;
	}
	return stos[gora];
}

template <class typ>
void TabStos<typ>::push(const typ & wart) {
	if (gora == pojemnosc - 1) {
		FullStackExcpetion fullE;
		throw fullE;
	}
	stos[++gora] = wart;
}

template <class typ>
typ TabStos<typ>::pop() {
	if (isEmpty()) {
		EmptyStackException emptyE;
		throw emptyE;
	}
	return stos[gora--];
}

template <class typ>
void TabStos<typ>::show() const {
	for (int i = gora; i >= 0; i--)
		cout << stos[i] << endl;
}

template <class typ>
void TabStos<typ>::deleteAll() {
	if (isEmpty()) {
		EmptyStackException emptyE;
		throw emptyE;
	}
	gora = -1;
	delete[] stos;
}
