
#pragma once
#include <stack>
#include "wyjatki.h"

using namespace std;

template <class typ>
class StlStos
{
	stack <typ> stos;
public:
	//sprawdza czy stos jest pusty
	//true - jest pusty
	//false - nie jest pusty
	bool isEmpty() {
		return stos.empty();
	}

	//zwraca element g�ry stosu lub wyrzuca wyj�tek EmptyStackException kiedy stos jest pusty
	typ & top() const {
		return stos.top();
	}

	//dodaje elelemnt do stosu lub wyrzuca wy�tek FullStackException kiedy stos jest pe�ny
	void push(const typ & wart) {
		stos.push(wart);
	}

	//�ciaga element ze stosu lub wyrzuca wyj�tek EmptyStackException kiedy stos jest pusty
	typ pop();

	//zwraca ilo�� element�w na stosie
	int size() {
		return stos.size();
	}

	//wyswietla ca�y stos na standardowym wyj�ciu
	void show();

	//usuwa wszystkie elementy ze stosu
	void deleteAll() {
		while (!stos.empty())
			stos.pop();
	}
};

template <class typ>
typ StlStos<typ>::pop() {
	if (stos.empty()) {
		EmptyStackException wyjatek;
		throw wyjatek;
	}
	typ tmp = stos.top();
	stos.pop();
	return tmp;
}

template <class typ>
void StlStos<typ>::show() {
	stack <typ> tmp = stos;
	while (!tmp.empty())
	{
		cout << tmp.top() << endl;
		tmp.pop();
	}
}

